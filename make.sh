#!/bin/bash

# Othello: classic game based on GTK+
# Copyright (C) 2015-2017  Balázs Dura-Kovács

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



# http://stackoverflow.com/questions/410980/include-a-text-file-in-a-c-program-as-a-char

#xxd -i < o1.glade > o1.xxd
#echo ', 0' >> o1.xxd

# https://developer.gnome.org/gio/2.32/glib-compile-resources.html

glib-compile-resources --target resources.h --generate  resources.xml
glib-compile-resources --target resources.c --generate  resources.xml

rm othello

gcc -o othello othello.c resources.c -Wall `pkg-config --cflags --libs gtk+-3.0` -export-dynamic